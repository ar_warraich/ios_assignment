//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import XCTest
import RxSwift
import RxCocoa
@testable import Scalio

class ProfilesRepositoryMock: ProfilesRepositoryProtocol {

    func fetchProfiles(query: String, page: Int) -> Observable<Users> {
        return Observable.create { observer in
            let users = FetchProfilesUseCaseTests().users
            observer.onNext(users)
            observer.onCompleted()
            return Disposables.create()
        }
    }
}

class FetchProfilesUseCaseTests: XCTestCase {
    lazy var users: Users = {
        let profiles = MockDataGenerator().mockProfilesData()
        return profiles
    }()
    
    func testFetchProfilesUseCase_whenSuccessfullyFetchesProfiles_profilesArrayShouldHaveCountEqualToNine_succeeds() {
        // given
        let expectation = self.expectation(description: "Download profiles list")
        expectation.expectedFulfillmentCount = 1
        let profilesRepository = ProfilesRepositoryMock()
        let useCase = FetchProfilesUseCase(profilesRepository: profilesRepository)
        var profiles = Users()
        
        // when
        useCase.execute(query: "login", page: 1)
            .subscribe { event in
            
            switch event {
            case .next(let users):
                profiles = users
            case .error(_):
                expectation.fulfill()
            case .completed:
                expectation.fulfill()
                break
            }
        }.disposed(by: DisposeBag())
        
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(profiles.items?.count, 9)
    }
}
