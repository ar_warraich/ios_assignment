//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation
@testable import Scalio

class MockDataGenerator {
    func mockProfilesData() -> Users {
        let bundle = Bundle(for: Self.self)
        if let url = bundle.url(forResource: "MockData", withExtension: "json") {
            let data = try? Data(contentsOf: url)
            let decoder = JSONDecoder()
            do {
                let profiles = try decoder.decode(Users.self, from: data!)
                return profiles
            } catch let err {
                print(err)
            }
        }
       return Users()
    }
}
