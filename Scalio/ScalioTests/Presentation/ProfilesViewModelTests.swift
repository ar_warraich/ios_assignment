//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import XCTest
import RxSwift
import RxCocoa
@testable import Scalio

class ProfilesViewModelTests: XCTestCase {
    
    lazy var users: Users = {
        let profiles = MockDataGenerator().mockProfilesData()
        return profiles
    }()
    
    class FetchProfilesUseCaseMock: FetchProfilesUseCaseProtocol {
      
        func execute(query: String, page: Int) -> Observable<Users> {
            return Observable.create { observer in
                let users = ProfilesViewModelTests().users
                observer.onNext(users)
                observer.onCompleted()
                return Disposables.create()
            }
        }
    }
    
    // MARK: - ProfilesDownloaderDelegate
    
    func testWhenFetchProfilesUseCaseRecievesProfiles_thenViewModelProfilesCountIsEqualToNine_succeeds() {
        // given
        let expectation = self.expectation(description: "Fetch profiles list")
        expectation.expectedFulfillmentCount = 1
        let fetchProfilesUseCaseMock = FetchProfilesUseCaseMock()
        let viewModel = ProfilesViewModel(fetchProfilesUseCase: fetchProfilesUseCaseMock)
        
        // when
        viewModel.fetchUsersProfile()
        expectation.fulfill()
        
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual((viewModel.users.items?.count).orZero, 9)
    }
}
