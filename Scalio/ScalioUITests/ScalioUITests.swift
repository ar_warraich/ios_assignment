//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import XCTest
@testable import Scalio

class ScalioUITests: XCTestCase {
    
    let app = XCUIApplication()
    
    override func setUpWithError() throws {
        continueAfterFailure = false
        app.launch()
    }
    
    func testSubmitButtonAction_whenTapped_thenShouldSearchWithGivenText_displaysResults_succeeds() {
        
        let textField = app.textFields[AccessibilityIdentifier.searchTextFieldIdentifier]
        XCTAssert(textField.exists, "search text field doesn't exist" )
        _ = textField.waitForExistence(timeout: 10.0)
        
        textField.tap()
        textField.typeText("Login")
        XCTAssert(textField.exists, "tf exists" )
        
        XCTAssertEqual( (textField.value as? String), "Login", "TextField has proper value" )
        
        let submitDutton = app.buttons[AccessibilityIdentifier.submitButtonIdentifier]
        XCTAssertTrue(submitDutton.exists)
        
        submitDutton.tap()
        _ = submitDutton.waitForExistence(timeout: 10)
        
        let profileCell = app.tables.cells[AccessibilityIdentifier.profileCellIdentifier].firstMatch
        _ = profileCell.waitForExistence(timeout: 10.0)
        
        XCTAssertTrue(profileCell.exists)
        profileCell.tap()
        
        let scoreLabel = app.staticTexts[AccessibilityIdentifier.profileDetailScoreLabelIdentifier].firstMatch
        XCTAssert(scoreLabel.exists)
        _ = scoreLabel.waitForExistence(timeout: 10)
        
        XCTAssertNotNil(scoreLabel.value)
    }
}
