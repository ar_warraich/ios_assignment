//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

final class UserProfileDetailDIContainner {
    
    struct Dependencies {
        let userProfileInfo: Items
    }
    
    private let dependencies: Dependencies
    
    // MARK: - Init
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    // MARK: - ViewModel
    
    private func makeFetchUserProfileDetailViewModel() -> UserProfileDetailViewModel {
        return UserProfileDetailViewModel(userProfileInfo: dependencies.userProfileInfo)
    }
    
    // MARK: - ViewController
    
    func makeSplashViewController(coordinator: MainCoordinator) -> UserProfileDetailViewController {
        let userProfileDetailViewController = UserProfileDetailViewController(coordinator: coordinator, viewModel: makeFetchUserProfileDetailViewModel())
        return userProfileDetailViewController
    }
}
