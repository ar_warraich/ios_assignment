//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

final class UserProfileDIContainer {
    
    struct Dependencies {
        let networkService: NetworkService
    }
    
    private let dependencies: Dependencies
    
    // MARK: - Init
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    // MARK: - UseCases
    
    private func fetchProfilesUseCase() -> FetchProfilesUseCaseProtocol {
        return FetchProfilesUseCase(profilesRepository: makeProfilesRepository())
    }
    
    // MARK: - Repository
    
    private func makeProfilesRepository() -> ProfilesRepositoryProtocol {
        return ProfilesRepository(networkService: dependencies.networkService)
    }
    
    // MARK: - ViewModel
    
    private func makeFetchUserProfileLaunchesViewModel() -> ProfilesViewModel {
        return ProfilesViewModel(fetchProfilesUseCase: fetchProfilesUseCase())
    }
    
    // MARK: - ViewController
    
    func makeProfilesViewController(coordinator: MainCoordinator) -> ProfilesViewController {
        let profilesLaunchListVC = ProfilesViewController(coordinator: coordinator, viewModel: makeFetchUserProfileLaunchesViewModel())
        return profilesLaunchListVC
    }
}
