//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import UIKit

protocol Coordinator {
    associatedtype Destination
    
    @discardableResult func start(from destination: Destination) -> UIViewController?
    @discardableResult func restart(from destination: Destination) -> UIViewController?
    func set(to destination: Destination)
    func push(to destination: Destination)
    func pop()
    func present(controller viewController: UIViewController)
    func dismiss(animated: Bool, completion: @escaping () -> Void)
    func makeViewController(for destination: Destination) -> UIViewController
}

extension Coordinator {
    func set(to destination: Destination) {
        fatalError("Coordinator has not implemented set(to destination: Destination) function")
    }
    
    @discardableResult func restart(from destination: Destination) -> UIViewController? {
        fatalError("Coordinator has not implemented restart(from destination: Destination) function")
    }
}
