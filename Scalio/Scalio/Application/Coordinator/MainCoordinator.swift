//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import UIKit

final class MainCoordinator: NSObject, Coordinator {
    
    enum Destination {
        case splash
        case profiles
        case userProfileDetail(_ userProfile: Items)
        case alert(_ title: String? = nil, description: String?, cancelButtonTitle:String? = nil, action: ((UIAlertAction) -> Void)? = nil)
    }
    
    weak private(set) var navigationController: UINavigationController?
    var appDIContainer: AppDIContainer
    
    // MARK: - Initializer
    init(navigationController: UINavigationController, appDIContainer: AppDIContainer) {
        self.navigationController = navigationController
        self.appDIContainer = appDIContainer
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    deinit {
        print("\nDeinit: \(self.classForCoder)\n")
    }
    
    // Starts the flow of the application
    @discardableResult func start(from destination: Destination) -> UIViewController? {
        set(to: destination)
        return self.navigationController
    }
    
    @discardableResult func restart(from destination: Destination) -> UIViewController? {
        set(to: destination)
        return self.navigationController
    }
    
    // MARK: - Navigator
    func set(to destination: Destination) {
        let viewController = makeViewController(for: destination)
        DispatchQueue.main.async { [weak self] in
            self?.navigationController?.setViewControllers([viewController], animated: false)
        }
    }
    
    func push(to destination: Destination) {
        let viewController = makeViewController(for: destination)
        DispatchQueue.main.async { [weak self] in
            self?.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func pop() {
        DispatchQueue.main.async { [weak self] in
            _ = self?.navigationController?.popViewController(animated: true)
        }
    }
    
    func present(controller viewController: UIViewController) {
        DispatchQueue.main.async { [weak self] in
            self?.navigationController?.present(viewController, animated: true)
        }
    }
    
    func present(to destination: Destination, modalPresentationStyle: UIModalPresentationStyle? = nil){
        if self.navigationController?.presentedViewController != nil {
            print("View controller is already presented")
            return
        }
        let viewController = makeViewController(for: destination)
        if let modalPresentationStyle = modalPresentationStyle {
            viewController.modalPresentationStyle = modalPresentationStyle
        }
        self.present(controller: viewController)
    }
    
    func dismiss(animated: Bool = true, completion: @escaping () -> Void = {}) {
        self.navigationController?.dismiss(animated: animated, completion: completion)
    }
    
    // MARK: - internal
    internal func makeViewController(for destination: Destination) -> UIViewController {
        switch destination {
            
        case .splash:
            let splashDIContainer = appDIContainer.makeSplashDIContainer()
            let splashViewController = splashDIContainer.makeSplashViewController(coordinator: self)
            return splashViewController
            
        case .profiles:
            let profilesDIConatiner = appDIContainer.makeProfilesDIContainer()
            let profilesViewController = profilesDIConatiner.makeProfilesViewController(coordinator: self)
            return profilesViewController
            
        case .userProfileDetail(let userProfile):
            let profilesDIConatiner = appDIContainer.makeUserProfileDetailDIContainer(userProfileInfo: userProfile)
            let profilesViewController = profilesDIConatiner.makeSplashViewController(coordinator: self)
            return profilesViewController
            
        case .alert(let title, description: let description, cancelButtonTitle: let cancelButtonTitle, action: let actionHandler):
            
            let alert = UIAlertController(title: title ?? Strings.Error.error, message: ((description?.isEmpty ?? true) ? Strings.Error.general : description), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: cancelButtonTitle ?? Strings.Button.okay, style: .cancel, handler: actionHandler))
            return alert
        }
    }
}
