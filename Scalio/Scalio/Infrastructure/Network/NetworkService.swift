//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

protocol NetworkService {
    func request<Request: DataRequest>(_ request: Request, completion: @escaping (Result<Request.Response, Error>) -> Void)
}

final class ApplicationNetworkService: NetworkService {
    
    func request<Request: DataRequest>(_ request: Request, completion: @escaping (Result<Request.Response, Error>) -> Void) {
        
        guard var urlComponent = URLComponents(string: request.url) else {
            let error = NSError(
                domain: Strings.Error.invalidEndpoint,
                code: 404,
                userInfo: nil
            )
            
            return completion(.failure(error))
        }
        
        var queryItems: [URLQueryItem] = []
        
        request.queryItems.forEach {
            let urlQueryItem = URLQueryItem(name: $0.key, value: $0.value)
            urlComponent.queryItems?.append(urlQueryItem)
            queryItems.append(urlQueryItem)
        }
        
        urlComponent.queryItems = queryItems
        
        guard let url = urlComponent.url else {
            let error = NSError(
                domain: Strings.Error.invalidEndpoint,
                code: 404,
                userInfo: nil
            )
            
            return completion(.failure(error))
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = request.method.rawValue
        urlRequest.allHTTPHeaderFields = request.headers
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            if let error = error {
                self.showRequestDetailForFailure(responseObject: response as? HTTPURLResponse, error: error as NSError)
                return completion(.failure(error))
            }
            self.showRequestDetailForSuccess(responseObject: response as? HTTPURLResponse)
            
            guard let response = response as? HTTPURLResponse else {
                return completion(.failure(NSError.formate(message: Strings.Error.failedToParse)))
            }
            
            guard let data = data else {
                return completion(.failure(NSError.formate(message: Strings.Error.emptyResponse)))
            }
            
            if (200..<300 ~= response.statusCode) == false {
                
                do {
                    let decoder = JSONDecoder()
                    let decoded = try decoder.decode(ErrorResponse.self, from: data)
                    return completion(.failure(NSError.formate(response.statusCode, message: decoded.message)))
                    
                } catch let error as NSError {
                    return completion(.failure(error))
                }
            }
            
            do {
                let decodedData = try request.decode(data)
                completion(.success(decodedData))
            } catch let error as NSError {
                completion(.failure(error))
            }
        }
        .resume()
    }
    
    func showRequestDetailForSuccess(responseObject response : HTTPURLResponse?) {
        
#if DEBUG
        var logString :String = ""
        logString = "\n\n\n✅✅✅ ------- Success Response Start ------- ✅✅✅ \n"
        logString += ""+(response?.url?.absoluteString ?? "")
        logString += "\n✅✅✅ ------- Success Response End ------- ✅✅✅ \n\n\n"
        
        print(logString)
#endif
    }
    
    func showRequestDetailForFailure(responseObject response : HTTPURLResponse?, error:NSError) {
        
#if DEBUG
        var logString :String = ""
        logString = "\n\n\n❌❌❌❌ ------- Failure Response Start ------- ❌❌❌❌\n"
        logString += ""+(response?.url?.absoluteString ?? "")
        logString += "\n=========   Error   ========== \n" + error.description
        logString += "\n❌❌❌❌ ------- Failure Response End ------- ❌❌❌❌\n\n\n"
        
        print(logString)
#endif
        
    }
}
