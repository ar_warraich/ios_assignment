//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import UIKit

final class ImageDownloadService {
    private static var imageCache = NSCache<AnyObject, AnyObject>()
    
    class func getImage(urlString: String, completion: @escaping (UIImage?, String?) -> ()) {
        
        if let image = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            completion(image, urlString)
            
        } else {
            
            if let url = URL(string: urlString) {
                URLSession.shared.dataTask(with: url) {(data, response, error) in
                    if error == nil,
                       let data = data,
                       let image = UIImage(data: data) {
                        self.imageCache.setObject(image, forKey: (urlString as AnyObject))
                        
                        DispatchQueue.main.async(execute: { () -> Void in
                            completion(image, urlString)
                        })
                    } else {
                        completion(nil, nil)
                    }
                }.resume()
                
            } else {
                completion(nil, nil)
            }
        }
    }
}
