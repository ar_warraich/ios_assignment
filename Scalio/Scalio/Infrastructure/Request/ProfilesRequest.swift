//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

struct ProfilesRequest: DataRequest {
    
    var query: String
    var page: Int
    
    var url: String {
        var url: String = AppConfiguration.apiBaseURL
        url = url + AppConfiguration.Endpoint.search
        return url
    }
    
    var method: HTTPMethod {
        .get
    }
    
    var queryItems: [String : String] {
        return ["q": query,
                "page": "\(page)",
                "per_page": "\(AppConfiguration.paginationLimit)"]
    }
    
    func decode(_ data: Data) throws -> Users {
        let decoder = JSONDecoder()
        let response = try decoder.decode(Users.self, from: data)
        return response
    }
}
