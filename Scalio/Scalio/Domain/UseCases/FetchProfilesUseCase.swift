//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation
import RxSwift

protocol FetchProfilesUseCaseProtocol: AnyObject {
    func execute(query: String, page: Int) -> Observable<Users>
}

final class FetchProfilesUseCase: FetchProfilesUseCaseProtocol {
    private let profilesRepository: ProfilesRepositoryProtocol
    private let disposeBag = DisposeBag()
    
    // MARK: - Init
    
    init(profilesRepository: ProfilesRepositoryProtocol) {
        self.profilesRepository = profilesRepository
    }
    
    // MARK: - FetchProfilesUseCaseProtocol
    
    /// Executes the given useCase of fetching the Users
    ///
    func execute(query: String, page: Int) -> Observable<Users> {
        return Observable.create { [weak self] observer in
            self?.profilesRepository.fetchProfiles(query: query, page: page)
                .subscribe { event in
                    switch event {
                    case .next(let users):
                        observer.onNext(users)
                        observer.onCompleted()
                    case .error(let error):
                        observer.onError(error)
                    case .completed:
                        break
                    }
                }.disposed(by: self?.disposeBag ?? DisposeBag())
            return Disposables.create()
        }
    }
}
