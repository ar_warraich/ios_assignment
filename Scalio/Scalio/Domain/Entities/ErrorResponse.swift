//  Created on 26/02/2022.

import Foundation
struct ErrorResponse : Codable {
    let message : String?
    let documentation_url : String?

    enum CodingKeys: String, CodingKey {

        case message = "message"
        case documentation_url = "documentation_url"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        documentation_url = try values.decodeIfPresent(String.self, forKey: .documentation_url)
    }
}
