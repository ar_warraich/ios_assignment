//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation
import RxSwift

protocol ProfilesRepositoryProtocol: AnyObject {
    func fetchProfiles(query: String, page: Int) -> Observable<Users>
}

final class ProfilesRepository: ProfilesRepositoryProtocol {
    
    // MARK: - Variables
    
    private let networkService: NetworkService
    
    // MARK: - Init
    
    init(networkService: NetworkService) {
        self.networkService = networkService
    }
    
    /// Fetches the Users from the available dataSource
    ///
    func fetchProfiles(query: String, page: Int) -> Observable<Users> {
        // created a separate function keeping scalability in mind, if we need to cater for offline storage
        return Observable.create { observer in
            self.fetchProfilesFromInternet(query: query, page: page) { result in
                switch result {
                case .success(let users):
                    observer.onNext(users)
                    observer.onCompleted()
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    private func fetchProfilesFromInternet(query: String, page: Int, complete completion: @escaping (Result<Users, Error>) -> Void) {
        networkService.request(ProfilesRequest(query: query, page: page)) { result in
            switch result {
            case .success(let users):
                completion(.success(users))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
