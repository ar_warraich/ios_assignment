//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

struct Strings {
    
    struct Error {
        static let error = "Error"
        static let general = "Unexpected error occurred. Please try again."
        static let invalidEndpoint = "Ooops, there is something wrong with the endpoint"
        static let failedToParse = "Failed to parse network response"
        static let emptyResponse = "Empty response recived from server"
    }
    
    struct Common {
        //static let noData = "No Data found"
        static let search = "Search"
        static let submit = "Submit"
        static let enterSearchQuery = "Enter name and click on submit button to perform search."
        static let searchInProgress = "Search is in progress. Please wait."
        static let noData = "No data available. Enter name and click on submit button to perform search."
        static let score = "Score: %0.1f"
    }
    
    struct Button{
        static let okay = "Okay"
    }
}
