//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import UIKit

class BaseViewController: UIViewController {
    
    // MARK: - Initialization
    
    /**
     Initialize a new View Controller.
     */
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupLayoutConstraint()
        setupBindings()
    }
    
    deinit {
        print("\n>>>>>>>> Deinit: \(String(describing: self.classForCoder)) <<<<<<<<\n")
    }
    
    /// Set up the view's appearance
    func setupView() { }
    
    /// Set up the layout constraint
    func setupLayoutConstraint() { }
    
    /// Set up the bindings
    func setupBindings() { }
}
