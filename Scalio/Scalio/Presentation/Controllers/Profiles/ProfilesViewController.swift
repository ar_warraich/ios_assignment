//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import UIKit
import RxSwift
import RxCocoa

final class ProfilesViewController: BaseViewController {
    
    // MARK: - Properites
    private var coordinator: MainCoordinator
    private var viewModel: ProfilesViewModel
    private let disposeBag = DisposeBag()
    
    
    private lazy var searchContainorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .randomColor()
        return view
    }()
    
    private lazy var searchTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = Strings.Common.search
        textField.borderStyle = .roundedRect
        textField.returnKeyType = .search
        textField.autocorrectionType = .no
        textField.enablesReturnKeyAutomatically = true
        textField.accessibilityIdentifier = AccessibilityIdentifier.searchTextFieldIdentifier
        return textField
    }()
    
    private lazy var submitButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .appRed
        button.layer.cornerRadius = 8
        button.setTitle(Strings.Common.submit, for: .normal)
        button.titleLabel?.font = .appBold16
        button.accessibilityIdentifier = AccessibilityIdentifier.submitButtonIdentifier
        return button
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .clear
        tableView.rowHeight = UITableView.automaticDimension
        tableView.rowHeight = 108
        tableView.separatorColor = .clear
        tableView.register(cellType: UserInfoTableViewCell.self)
        return tableView
    }()
    
    // MARK: - Initialization
    init(coordinator: MainCoordinator, viewModel: ProfilesViewModel) {
        self.coordinator = coordinator
        self.viewModel = viewModel
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Methods
    override func setupView() {
        super.setupView()
        view.backgroundColor = .appWhite
        searchContainorView.addSubview(submitButton)
        searchContainorView.addSubview(searchTextField)
        
        self.view.addSubview(searchContainorView)
        self.view.addSubview(tableView)
    }
    
    override func setupLayoutConstraint() {
        super.setupLayoutConstraint()
        NSLayoutConstraint.activate([
            searchContainorView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            searchContainorView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            searchContainorView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 8),
            searchContainorView.heightAnchor.constraint(equalToConstant: 44),
            
            submitButton.rightAnchor.constraint(equalTo: searchContainorView.rightAnchor, constant: -8),
            submitButton.centerYAnchor.constraint(equalTo: self.searchTextField.centerYAnchor),
            submitButton.heightAnchor.constraint(equalToConstant: 34),
            submitButton.widthAnchor.constraint(equalToConstant: 120),
            
            searchTextField.leftAnchor.constraint(equalTo: searchContainorView.leftAnchor, constant: 8),
            searchTextField.centerYAnchor.constraint(equalTo: searchContainorView.centerYAnchor),
            searchTextField.rightAnchor.constraint(equalTo: submitButton.leftAnchor, constant: -8),
            
            tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            tableView.topAnchor.constraint(equalTo: searchContainorView.bottomAnchor, constant: 8),
            tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    override func setupBindings() {
        super.setupBindings()
        
        searchTextField.rx.controlEvent(.editingChanged)
            .asObservable()
            .subscribe(onNext: { [weak self] _ in
                if let text = self?.searchTextField.text {
                    self?.viewModel.searchQuery = text
                }
            })
            .disposed(by: disposeBag)
        
        searchTextField.rx.controlEvent(.editingDidEndOnExit)
            .asObservable()
            .subscribe(onNext: { [weak self] _ in
                self?.submitButton.sendActions(for: .touchUpInside)
            })
            .disposed(by: disposeBag)
        
        submitButton.rx.controlEvent(.touchUpInside)
            .asDriver().drive {[weak self] _ in
                _ = self?.searchTextField.resignFirstResponder()
                self?.viewModel.resetAndFetch()
            }
            .disposed(by: disposeBag)
        
        viewModel.isLoading
            .observe(on: MainScheduler.instance)
            .subscribe { [weak self] isLoading in
                guard let self = self else { return }
                if isLoading.element.orFalse {
                    self.view.showActivityIndicator()
                } else {
                    self.view.hideActivityIndicator()
                }
            }.disposed(by: disposeBag)
        
        viewModel.dataSource
            .observe(on: MainScheduler.instance)
            .bind(to: tableView.rx.items(cellIdentifier: UserInfoTableViewCell.identifier, cellType: UserInfoTableViewCell.self)) { index, item, cell in
                
                cell.configure(data: item)
            }.disposed(by: disposeBag)
        
        viewModel.dataFetchingStatus
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] state in
                guard let self = self else { return }
                if state == .dataFound {
                    self.tableView.hideEmptyStateMessage()
                } else {
                    self.tableView.showEmptyStateMessage(description: state.description)
                }
                
            }).disposed(by: disposeBag)
        
        tableView.rx.modelSelected(Items.self)
            .subscribe(onNext: { [weak self] selectedUserProfile in
                self?.coordinator.present(to: .userProfileDetail(selectedUserProfile))
            })
            .disposed(by: disposeBag)
        
        tableView.rx.didEndDecelerating.subscribe { [weak self] _ in
            guard let self = self else { return }
            let scrolledDistance = self.tableView.contentOffset.y + self.tableView.frame.height
            let percentage = (scrolledDistance/self.tableView.contentSize.height) * 100
            if percentage >= 80 &&
                self.viewModel.isMoreDataAvailable() {
                self.viewModel.fetchUsersProfile()
            }
        }.disposed(by: disposeBag)
        
        viewModel.errorMessage
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] error in
                self?.coordinator.present(to: .alert(description: error))
            }).disposed(by: disposeBag)
    }
}
