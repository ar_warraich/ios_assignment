//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import UIKit

final class UserInfoTableViewCell: UITableViewCell {
    
    // MARK: - Properites
    private var item: Items?
    private let containerView: BaseView = {
        let view = BaseView()
        view.layer.cornerRadius = 8
        view.layer.borderWidth = 0.5
        view.backgroundColor = UIColor.randomColor()
        view.layer.borderColor = view.backgroundColor?.withAlphaComponent(0.5).cgColor
        return view
    }()
    
    private let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 42
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private let nameLabel: UILabel = {
        let nameLabel = UILabel()
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.font = .appBold20
        nameLabel.numberOfLines = 2
        return nameLabel
    }()
    
    private let typeLabel: UILabel = {
        let typeLabel = UILabel()
        typeLabel.translatesAutoresizingMaskIntoConstraints = false
        typeLabel.font = .appRegular14
        typeLabel.numberOfLines = 2
        return typeLabel
    }()
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        accessibilityIdentifier = AccessibilityIdentifier.profileCellIdentifier
        self.selectionStyle = .none
        self.addSubview(containerView)
        containerView.addSubview(profileImageView)
        containerView.addSubview(nameLabel)
        containerView.addSubview(typeLabel)
        self.setupLayoutConstraint()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        profileImageView.image = nil
        containerView.backgroundColor = UIColor.randomColor()
        containerView.layer.borderColor = containerView.backgroundColor?.withAlphaComponent(0.5).cgColor
    }
    
    // MARK: - Methods
    private func setupLayoutConstraint() {
        NSLayoutConstraint.activate([
            containerView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            containerView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8),
            containerView.topAnchor.constraint(equalTo: self.topAnchor, constant: 4),
            containerView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -4),
            
            profileImageView.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 8),
            profileImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 8),
            profileImageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -8),
            profileImageView.widthAnchor.constraint(equalTo: profileImageView.heightAnchor, multiplier: 1),
            
            nameLabel.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: 16),
            nameLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -16),
            nameLabel.topAnchor.constraint(greaterThanOrEqualTo: profileImageView.topAnchor),
            nameLabel.bottomAnchor.constraint(equalTo: profileImageView.centerYAnchor, constant: -4),
            
            typeLabel.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: 16),
            typeLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -16),
            typeLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 8),
            typeLabel.bottomAnchor.constraint(lessThanOrEqualTo: containerView.bottomAnchor),
        ])
    }
    
    func configure(data: Items) {
        item = data
        nameLabel.text = data.login
        typeLabel.text = data.type
        if let imageUrl = data.avatar_url {
            ImageDownloadService.getImage(urlString: imageUrl) { [weak self] image, imageUrl in
                guard let self = self,
                      self.item?.avatar_url == imageUrl
                else { return }
                self.profileImageView.image = image
            }
        }
    }
}
