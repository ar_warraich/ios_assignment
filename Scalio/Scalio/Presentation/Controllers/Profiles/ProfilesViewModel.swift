//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation
import RxSwift
import RxRelay

final class ProfilesViewModel {
    enum DataFetchStatus {
        case dataFound
        case enterSearchQuery
        case searchInProgress
        case noData
        
        var description: String {
            switch self {
                
            case .dataFound:
                return ""
            case .enterSearchQuery:
                return Strings.Common.enterSearchQuery
            case .searchInProgress:
                return Strings.Common.searchInProgress
            case .noData:
                return Strings.Common.noData
            }
        }
    }
    
    private let fetchProfilesUseCase: FetchProfilesUseCaseProtocol
    private let disposeBag = DisposeBag()
    
    let isLoading: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let dataSource: PublishSubject<[Items]> = PublishSubject()
    let errorMessage: PublishSubject<String> = PublishSubject()
    let dataFetchingStatus = BehaviorSubject<DataFetchStatus>(value: .enterSearchQuery)
    
    
    private(set) var users = Users()
    private var page = 1
    var searchQuery = ""
    
    // MARK: - Init
    init(fetchProfilesUseCase: FetchProfilesUseCaseProtocol) {
        self.fetchProfilesUseCase = fetchProfilesUseCase
    }
    
    // MARK: - Functions
    func resetAndFetch() {
        page = 1
        self.users.items = []
        self.users.total_count = 0
        dataSource.onNext([])
        
        fetchUsersProfile()
    }
    
    func fetchUsersProfile() {
        if isLoading.value == true {
            print("Loading is already in process")
            return
        }
        isLoading.accept(true)
        self.dataFetchingStatus.onNext(.searchInProgress)
        fetchProfilesUseCase.execute(query: searchQuery, page: page)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                self.isLoading.accept(false)
                
                switch event {
                case .next(let users):
                    self.handleResponse(users: users)
                case .error(let error):
                    self.dataFetchingStatus.onNext(.noData)
                    self.errorMessage.onNext(error.localizedDescription)
                case .completed:
                    break
                }
            }.disposed(by: disposeBag)
    }
    
    private func handleResponse(users: Users) {
        if users.total_count.orZero <= 0 {
            self.dataFetchingStatus.onNext(.noData)
        } else {
            self.dataFetchingStatus.onNext(.dataFound)
            if page == 1 {
                self.users.items = users.items
                self.users.total_count = users.total_count
            } else {
                self.users.items?.append(contentsOf: users.items ?? [])
            }
            
            
            /*
             Sorting capability should have been provided by the API, but implementing it on the client end because API does not support it.
             
             As we have pagination and sorting on client end will lead to a bad UX.
             */
            self.users.items?.sort(by: {$0.login.orEmpty < $1.login.orEmpty})
            
            dataSource.onNext(self.users.items ?? [])
            page += 1
        }
    }
    
    func isMoreDataAvailable() -> Bool {
        return (users.items?.count).orZero < users.total_count.orZero
    }
}
