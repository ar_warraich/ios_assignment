//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import RxSwift
import RxCocoa
import UIKit

final class UserProfileDetailViewController: BaseViewController {
    
    // MARK: - Properites
    private var coordinator: MainCoordinator
    private var viewModel: UserProfileDetailViewModel
    private let disposeBag = DisposeBag()
    
    private let contentView: BaseView = {
        let view = BaseView()
        view.backgroundColor = UIColor.randomColor()
        return view
    }()
    
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let detailContentView: BaseView = {
        let view = BaseView()
        view.backgroundColor = .appBlack?.withAlphaComponent(0.4)
        return view
    }()
    
    private let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .randomColor()
        imageView.layer.cornerRadius = 25
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .appWhite
        label.font = UIFont.appBold20
        return label
    }()
    
    private let scoreLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.setContentHuggingPriority(UILayoutPriority(rawValue: 300), for: .horizontal)
        label.textColor = .appWhite
        label.accessibilityIdentifier = AccessibilityIdentifier.profileDetailScoreLabelIdentifier
        return label
    }()
    
    // MARK: - Initialization
    init(coordinator: MainCoordinator, viewModel: UserProfileDetailViewModel) {
        self.coordinator = coordinator
        self.viewModel = viewModel
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Methods
    override func setupView() {
        super.setupView()
        view.backgroundColor = .appWhite
        self.view.addSubview(contentView)
        contentView.addSubview(imageView)
        contentView.addSubview(detailContentView)
        
        detailContentView.addSubview(profileImageView)
        detailContentView.addSubview(nameLabel)
        detailContentView.addSubview(scoreLabel)
        
        nameLabel.text = viewModel.userProfileInfo.login
        scoreLabel.text = String(format: Strings.Common.score, viewModel.userProfileInfo.score.orZero)
        
        if let imageUrl = viewModel.userProfileInfo.avatar_url {
            ImageDownloadService.getImage(urlString: imageUrl) { [weak self] image, _ in
                self?.profileImageView.image = image
            }
        }
        
        if let imageUrl = viewModel.userProfileInfo.avatar_url {
            ImageDownloadService.getImage(urlString: imageUrl) { [weak self] image, _ in
                self?.imageView.image = image
            }
        }
    }
    
    override func setupLayoutConstraint() {
        super.setupLayoutConstraint()
        NSLayoutConstraint.activate([
            contentView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            contentView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            contentView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            
            imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            detailContentView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            detailContentView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            detailContentView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            detailContentView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.12),
            
            profileImageView.leadingAnchor.constraint(equalTo: detailContentView.leadingAnchor, constant: 8),
            profileImageView.topAnchor.constraint(equalTo: detailContentView.topAnchor, constant: 8),
            profileImageView.heightAnchor.constraint(equalToConstant: 50),
            profileImageView.widthAnchor.constraint(equalToConstant: 50),
            
            nameLabel.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 8),
            nameLabel.trailingAnchor.constraint(lessThanOrEqualTo: detailContentView.trailingAnchor, constant: -8),
            nameLabel.topAnchor.constraint(equalTo: profileImageView.topAnchor, constant: 4),
            
            scoreLabel.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 8),
            scoreLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 4)
        ])
    }
}
