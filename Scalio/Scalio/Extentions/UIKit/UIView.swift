//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import UIKit

extension UIView {
    
    func showEmptyStateMessage(description: String?,
                               descriptionColor :UIColor? = UIColor.appDarkGray,
                               descriptionFont :UIFont = UIFont.appRegular14) {
        
        if let emptyStateMessage = self.viewWithTag(998877) as? UILabel {
            emptyStateMessage.text = description
        } else {
            let emptyStateMessage: UILabel = UILabel()
            emptyStateMessage.translatesAutoresizingMaskIntoConstraints = false
            emptyStateMessage.text = description
            emptyStateMessage.textColor = descriptionColor
            emptyStateMessage.numberOfLines = 0
            emptyStateMessage.textAlignment = .center
            emptyStateMessage.font = descriptionFont
            emptyStateMessage.tag = 998877
            
            self.addSubview(emptyStateMessage)
            self.bringSubviewToFront(emptyStateMessage)
            
            NSLayoutConstraint.activate([
                emptyStateMessage.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10),
                emptyStateMessage.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10),
                emptyStateMessage.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0),
                emptyStateMessage.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0),
            ])
            
        }
    }
    
    func hideEmptyStateMessage() {
        if let emptyStateMessage = self.viewWithTag(998877) as? UILabel {
            
            emptyStateMessage.isHidden = true
            emptyStateMessage.removeFromSuperview()
        }
    }
    
    func showActivityIndicator() {
        if let containorView = self.viewWithTag(778899),
           let activityIndicator = containorView.viewWithTag(332211) as? UIActivityIndicatorView {
            activityIndicator.startAnimating()
            
        } else {
            let containorView = UIView()
            containorView.translatesAutoresizingMaskIntoConstraints = false
            containorView.backgroundColor = .appBlack?.withAlphaComponent(0.7)
            containorView.layer.cornerRadius = 8
            containorView.tag = 778899
            
            let activityIndicator = UIActivityIndicatorView(style: .large)
            activityIndicator.translatesAutoresizingMaskIntoConstraints = false
            activityIndicator.startAnimating()
            activityIndicator.color = .appWhite
            activityIndicator.tag = 332211
            
            containorView.addSubview(activityIndicator)
            self.addSubview(containorView)
            self.bringSubviewToFront(containorView)
            
            NSLayoutConstraint.activate([
                activityIndicator.centerYAnchor.constraint(equalTo: containorView.centerYAnchor),
                activityIndicator.centerXAnchor.constraint(equalTo: containorView.centerXAnchor),
                
                containorView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
                containorView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
                containorView.widthAnchor.constraint(equalToConstant: 80),
                containorView.heightAnchor.constraint(equalToConstant: 80)
                
            ])
        }
        
    }
    
    func hideActivityIndicator() {
        if let containorView = self.viewWithTag(778899) {
            containorView.removeFromSuperview()
        }
    }
}
