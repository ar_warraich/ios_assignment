//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import UIKit

protocol Identificable { static var identifier: String { get } }

extension Identificable {
    static var identifier: String { return String.init(describing: Self.self) }
}

extension UITableViewCell: Identificable {}

extension UITableView {
    
    func register<T:UITableViewCell>(cellType:T.Type)  {
        self.register(cellType.self, forCellReuseIdentifier: cellType.identifier)
    }
    
}
