//
// Copyright (c) 2022, Scalio, https://scal.io
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import UIKit

extension UIColor {
    
    static var appWhite: UIColor? {
        return .white
    }
    
    static var appBlack: UIColor? {
        return .black
    }
    
    static var appDarkGray: UIColor? {
        return UIColor(named: "appDarkGray")
    }
    
    static var appRed: UIColor? {
        return UIColor(named: "appRed")
    }
    
    private static let randomColors = [
        UIColor(red: 229/255, green: 227/255, blue: 201/255, alpha: 0.5),
        UIColor(red: 180/255, green: 207/255, blue: 176/255, alpha: 0.3),
        UIColor(red: 120/255, green: 147/255, blue: 149/255, alpha: 0.3),
        UIColor(red: 226/255, green: 194/255, blue: 185/255, alpha: 0.3),
        UIColor(red: 120/255, green: 130/255, blue: 164/255, alpha: 0.3)
    ]
    
    static func randomColor() -> UIColor? {
        return randomColors.randomElement()
    }
}
