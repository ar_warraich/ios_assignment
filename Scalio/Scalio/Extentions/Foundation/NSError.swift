//  Created on 26/02/2022.

import Foundation
extension NSError {
    class func formate(_ code: Int = 404, message: String?) -> NSError {
        return NSError(domain: AppConfiguration.domain, code: code, userInfo: [NSLocalizedDescriptionKey: message ?? Strings.Error.general])
    }
}
