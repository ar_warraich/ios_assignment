# Scalio

![Platform](https://img.shields.io/badge/Platform-iOS-orange.svg)
![Languages](https://img.shields.io/badge/Language-Swift-orange.svg)

First of all I would like to thank you for giving me the opportunity to show my technical skills by Implementing the provided coding challenge. I have completed the task that you have assigned to me. Please find the attached GitHub repo link.

## Demo
<img src="https://media.giphy.com/media/F24wlKjXhYMxLkI1gB/giphy.gif" width="222" height="480" />

## Features

- [x] Implemented Search feature using GitHub API.
- [x] CLEAN Architecture 
- [x] DIContainers
- [x] Implemented MVVM-C
- [x] Network Requests using URLSession
- [x] Reactive Bindings using RxSwift
- [x] Image Caching
- [x] Unit Tests
- [x] UI Tests
- [x] Zero warnings

## Requirements

- iOS 15+
- Xcode 13.2+
- Swift 5+

## Clean Architecture using MVVM-C

In Clean Architecture, we have different layers in the application. The main rule is not to have dependencies from inner layers to outer layers. After grouping all layers we have: Presentation, Domain and Data layers.

- #### Domain Layer
    - Domain Layer (Business logic) is the innermost part of the onion (without dependencies to other layers, it is totally isolated). It contains Entities(Business Models)
    
- #### Presentation Layer 
    - Presentation Layer contains UI (UIViewControllers or SwiftUI Views). Views are coordinated by ViewModels (Presenters). Presentation Layer depends only on the Domain Layer
    
- #### Data Layer
    - Data Layer contains Repository Implementations and one or many Data Sources. Repositories are responsible for coordinating data from different Data Sources. Data Source can be Remote or Local (for example persistent database). The Data Layer depends only on the Domain Layer. In this layer, we can also add mapping of Network JSON Data (e.g. Decodable conformance) to Domain Models

- #### Dependency Direction
    - Presentation Layer, Domain Layer, Data Repositories Layer
    - Presentation Layer (MVVM) = ViewModels(Presenters) + Views(UI)
    - Domain Layer = Entities
    - Data Repositories Layer = Repositories Implementations + API(Network) + Persistence DB

## Dependencies

- CocoaPods
- RxSwift

## Installation

- Open the Scalio.xcworkspace file and run the app on the simulator.

